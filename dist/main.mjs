// src/main.ts
import "dotenv/config";

// src/context/context.ts
import { AsyncLocalStorage } from "node:async_hooks";
var Context = class {
  _context = new AsyncLocalStorage();
  get store() {
    return this._context.getStore();
  }
  get context() {
    return this._context;
  }
  setToStore(key, value) {
    const store = this._context.getStore();
    if (!store)
      throw new Error("Init context first");
    store[key] = value;
  }
};
var context = new Context();

// src/logger/pino.logger.ts
import { pino } from "pino";
var PinoLogger = class {
  logger;
  constructor({
    prettyPrint = false,
    destStreams
  }) {
    const pinoOptions = {};
    if (prettyPrint) {
      pinoOptions.transport = {
        target: "pino-pretty",
        options: {
          colorize: true
        }
      };
    }
    let pinoStreams = [
      { level: "info", stream: process.stdout },
      { level: "error", stream: process.stderr }
    ];
    if (destStreams?.length) {
      pinoStreams = destStreams;
    }
    this.logger = pino(
      pinoOptions,
      pino.multistream(pinoStreams, { dedupe: true })
    );
  }
  error(message, metadata) {
    if (metadata) {
      this.logger.error(metadata, message);
    } else {
      this.logger.error(message);
    }
  }
  info(message, metadata) {
    if (metadata) {
      this.logger.info(metadata, message);
    } else {
      this.logger.info(message);
    }
  }
};

// src/logger/logger.ts
var isObject = (data) => {
  return typeof data === "object" && data !== null && !Array.isArray(data);
};
var Logger = class {
  _logger = null;
  get logger() {
    if (!this._logger)
      throw new Error("Init logger first");
    return this._logger;
  }
  joinContextAndMetadata(metadata) {
    let m;
    if (metadata instanceof Error)
      m = {
        ...metadata,
        stack: metadata.stack,
        message: metadata.message,
        name: metadata.name
      };
    else if (isObject(metadata))
      m = metadata;
    else
      m = { metadata };
    return { ...context.store, ...m };
  }
  init(loggerConstructorOption) {
    this._logger = new PinoLogger(loggerConstructorOption);
  }
  error(message, metadata) {
    this.logger.error(message, this.joinContextAndMetadata(metadata));
  }
  info(message, metadata) {
    this.logger.info(message, this.joinContextAndMetadata(metadata));
  }
};
var logger = new Logger();

// src/env/env.ts
import convict from "convict";
var Env = class {
  convictConfig;
  init(schema) {
    this.convictConfig = convict(schema);
    this.convictConfig.validate({ allowed: "strict" });
  }
  get(key) {
    if (!this.convictConfig)
      throw Error("Init config first");
    return this.convictConfig.get(key);
  }
};
var env = new Env();
export {
  context,
  env,
  logger
};
