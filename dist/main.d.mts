import { Schema } from 'convict';
import { AsyncLocalStorage } from 'node:async_hooks';

type TLevel = 'error' | 'info';
interface IDestinationStream {
    write(msg: string): void;
}
interface IStreamEntry {
    stream: IDestinationStream;
    level: TLevel;
}
interface ILoggerConstructorOptions {
    prettyPrint: boolean;
    destStreams: IStreamEntry[];
}
interface ILogger {
    info(message: string, metadata?: unknown): void;
    error(message: string, metadata?: unknown): void;
}

declare class Logger implements ILogger {
    private _logger;
    private get logger();
    private joinContextAndMetadata;
    init(loggerConstructorOption: Partial<ILoggerConstructorOptions>): void;
    error(message: string, metadata?: unknown): void;
    info(message: string, metadata?: unknown): void;
}
declare const logger: Logger;

declare class Env<S = any> {
    private convictConfig?;
    init(schema: Schema<S>): void;
    get<T = string>(key: keyof S): T;
}
declare const env: Env<any>;

declare class Context {
    private readonly _context;
    get store(): Record<string, unknown> | undefined;
    get context(): AsyncLocalStorage<Record<string, unknown>>;
    setToStore(key: string, value: unknown): void;
}
declare const context: Context;

export { context, env, logger };
