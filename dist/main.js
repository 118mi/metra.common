"use strict";
var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// src/main.ts
var main_exports = {};
__export(main_exports, {
  context: () => context,
  env: () => env,
  logger: () => logger
});
module.exports = __toCommonJS(main_exports);
var import_config = require("dotenv/config");

// src/context/context.ts
var import_node_async_hooks = require("async_hooks");
var Context = class {
  _context = new import_node_async_hooks.AsyncLocalStorage();
  get store() {
    return this._context.getStore();
  }
  get context() {
    return this._context;
  }
  setToStore(key, value) {
    const store = this._context.getStore();
    if (!store)
      throw new Error("Init context first");
    store[key] = value;
  }
};
var context = new Context();

// src/logger/pino.logger.ts
var import_pino = require("pino");
var PinoLogger = class {
  logger;
  constructor({
    prettyPrint = false,
    destStreams
  }) {
    const pinoOptions = {};
    if (prettyPrint) {
      pinoOptions.transport = {
        target: "pino-pretty",
        options: {
          colorize: true
        }
      };
    }
    let pinoStreams = [
      { level: "info", stream: process.stdout },
      { level: "error", stream: process.stderr }
    ];
    if (destStreams?.length) {
      pinoStreams = destStreams;
    }
    this.logger = (0, import_pino.pino)(
      pinoOptions,
      import_pino.pino.multistream(pinoStreams, { dedupe: true })
    );
  }
  error(message, metadata) {
    if (metadata) {
      this.logger.error(metadata, message);
    } else {
      this.logger.error(message);
    }
  }
  info(message, metadata) {
    if (metadata) {
      this.logger.info(metadata, message);
    } else {
      this.logger.info(message);
    }
  }
};

// src/logger/logger.ts
var isObject = (data) => {
  return typeof data === "object" && data !== null && !Array.isArray(data);
};
var Logger = class {
  _logger = null;
  get logger() {
    if (!this._logger)
      throw new Error("Init logger first");
    return this._logger;
  }
  joinContextAndMetadata(metadata) {
    let m;
    if (metadata instanceof Error)
      m = {
        ...metadata,
        stack: metadata.stack,
        message: metadata.message,
        name: metadata.name
      };
    else if (isObject(metadata))
      m = metadata;
    else
      m = { metadata };
    return { ...context.store, ...m };
  }
  init(loggerConstructorOption) {
    this._logger = new PinoLogger(loggerConstructorOption);
  }
  error(message, metadata) {
    this.logger.error(message, this.joinContextAndMetadata(metadata));
  }
  info(message, metadata) {
    this.logger.info(message, this.joinContextAndMetadata(metadata));
  }
};
var logger = new Logger();

// src/env/env.ts
var import_convict = __toESM(require("convict"));
var Env = class {
  convictConfig;
  init(schema) {
    this.convictConfig = (0, import_convict.default)(schema);
    this.convictConfig.validate({ allowed: "strict" });
  }
  get(key) {
    if (!this.convictConfig)
      throw Error("Init config first");
    return this.convictConfig.get(key);
  }
};
var env = new Env();
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  context,
  env,
  logger
});
