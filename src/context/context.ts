import { AsyncLocalStorage } from 'node:async_hooks'

class Context {
  private readonly _context = new AsyncLocalStorage<Record<string, unknown>>()

  get store(): Record<string, unknown> | undefined {
    return this._context.getStore()
  }

  get context(): AsyncLocalStorage<Record<string, unknown>> {
    return this._context
  }

  setToStore(key: string, value: unknown): void {
    const store = this._context.getStore()
    if (!store) throw new Error('Init context first')
    // eslint-disable-next-line security/detect-object-injection
    store[key] = value
  }
}

export const context = new Context()
