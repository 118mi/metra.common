import convict, { Schema } from 'convict'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
class Env<S = any> {
  private convictConfig?: convict.Config<S>

  init(schema: Schema<S>): void {
    this.convictConfig = convict<S>(schema)
    this.convictConfig.validate({ allowed: 'strict' })
  }

  get<T = string>(key: keyof S): T {
    if (!this.convictConfig) throw Error('Init config first')

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return this.convictConfig.get(key) as T
  }
}

export const env = new Env()
