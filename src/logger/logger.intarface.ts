type TLevel = 'error' | 'info'

interface IDestinationStream {
  write(msg: string): void
}

interface IStreamEntry {
  stream: IDestinationStream
  level: TLevel
}

export interface ILoggerConstructorOptions {
  prettyPrint: boolean
  destStreams: IStreamEntry[]
}

export interface ILogger {
  info(message: string, metadata?: unknown): void
  error(message: string, metadata?: unknown): void
}
