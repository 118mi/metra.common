import { Logger as IPinoLogger, LoggerOptions, pino, StreamEntry } from 'pino'
import { ILogger, ILoggerConstructorOptions } from './logger.intarface'

export class PinoLogger implements ILogger {
  private readonly logger: IPinoLogger

  constructor({
    prettyPrint = false,
    destStreams,
  }: Partial<ILoggerConstructorOptions>) {
    const pinoOptions: LoggerOptions = {}
    if (prettyPrint) {
      pinoOptions.transport = {
        target: 'pino-pretty',
        options: {
          colorize: true,
        },
      }
    }

    let pinoStreams: StreamEntry[] = [
      { level: 'info', stream: process.stdout },
      { level: 'error', stream: process.stderr },
    ]
    if (destStreams?.length) {
      pinoStreams = destStreams
    }

    this.logger = pino(
      pinoOptions,
      pino.multistream(pinoStreams, { dedupe: true }),
    )
  }

  error(message: string, metadata?: unknown): void {
    if (metadata) {
      this.logger.error(metadata, message)
    } else {
      this.logger.error(message)
    }
  }

  info(message: string, metadata?: unknown): void {
    if (metadata) {
      this.logger.info(metadata, message)
    } else {
      this.logger.info(message)
    }
  }
}
