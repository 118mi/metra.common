import { context } from 'context'
import { PinoLogger } from './pino.logger'
import { ILogger, ILoggerConstructorOptions } from './logger.intarface'

const isObject = (data: unknown): data is Record<string, unknown> => {
  return typeof data === 'object' && data !== null && !Array.isArray(data)
}

class Logger implements ILogger {
  private _logger: ILogger | null = null

  private get logger(): ILogger {
    if (!this._logger) throw new Error('Init logger first')

    return this._logger
  }

  private joinContextAndMetadata(metadata: unknown): Record<string, unknown> {
    let m: Record<string, unknown>

    if (metadata instanceof Error)
      m = {
        ...metadata,
        stack: metadata.stack,
        message: metadata.message,
        name: metadata.name,
      }
    else if (isObject(metadata)) m = metadata
    else m = { metadata }

    return { ...context.store, ...m }
  }

  init(loggerConstructorOption: Partial<ILoggerConstructorOptions>): void {
    this._logger = new PinoLogger(loggerConstructorOption)
  }

  error(message: string, metadata?: unknown): void {
    this.logger.error(message, this.joinContextAndMetadata(metadata))
  }

  info(message: string, metadata?: unknown): void {
    this.logger.info(message, this.joinContextAndMetadata(metadata))
  }
}

export const logger = new Logger()
